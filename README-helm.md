helm create data-service
helm install --dry-run --debug ./data-service/
helm install --name example ./mychart/ --set service.type=NodePort

### Injecting varibles in command line
helm install --set param=value

### Pass value as file
helm install --values app-1.yaml

### Create our first Chart
helm create sodms

### Test the rendering of our template
helm template [NAME] [CHART] [flags]
helm template data-service sodms

### install our app using our chart
helm install data-service sodms --values ./sodms/data-service.values.yaml

### helm unistall
helm uninstall data-service

# upgrade our release
helm upgrade data-service sodms --values ./sodms/data-service.values.yaml
